#include <iostream>

#include <string.h>             // memset

#include <sys/ioctl.h>          // xioctl()
#include <sys/mman.h>           //mmap()
#include <unistd.h>             //close()
#include <fcntl.h>              // open(), O_RDWR
#include <sys/time.h>

//#include <linux/ioctl.h>
//#include <linux/types.h>
//#include <linux/v4l2-common.h>
//#include <linux/v4l2-controls.h>
#include <linux/videodev2.h>    // v4l2_capability

#include <opencv2/opencv.hpp>

struct Buffer {
    void* start;
    uint32_t bytes;
};

int xioctl(int fd, int request, void* arg) {
    int ret = 0;
    do {
        ret = xioctl(fd, request, arg);
        std::cout << ret << std::endl;
    }
    while (ret < 1 && EINTR == errno);

    return ret;
}

Buffer init(int fd) {
    std::cout << "Querying for device options ..." << std::endl;
    struct v4l2_capability cam_options;
    if (ioctl(fd, VIDIOC_QUERYCAP, &cam_options) < 0) {
        close(fd);
        throw std::runtime_error("No camera options are available!");
    }

    std::cout << "Device sepcifications: \n";
    std::cout << "Driver name: " << cam_options.driver << std::endl;
    std::cout << "Name of device: " << cam_options.card << std::endl;
    std::cout << "Location of the device: " << cam_options.bus_info << std::endl;

    auto major = (cam_options.version >> 16) & 0xFF;
    auto minor = (cam_options.version >> 8) & 0xFF;
    auto point_release = cam_options.version & 0xFF;
    std::cout << "Version of driver: " << major << "." << minor << "." << point_release << std::endl;

    if (!(cam_options.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        close(fd);
        throw std::runtime_error("Capturing devices could not be found!");
    }

    if (!(cam_options.capabilities & V4L2_CAP_STREAMING)) {
        close(fd);
        throw std::runtime_error("Device does not support streaming!!");
    }

    //v4l2-ctl -d /dev/video0 --list-formats-ext
    std::cout << "Setting up format ..." << std::endl;
    struct v4l2_format format;
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    format.fmt.pix.width = 640;
    format.fmt.pix.height = 480;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_GREY;
    format.fmt.pix.bytesperline = 640;

    if (ioctl(fd, VIDIOC_S_FMT, &format) < 0) { 
        close(fd);
        throw std::runtime_error("Failed to specify the format!"); 
    }

    std::cout << "Setting up FPS ..." << std::endl;
    struct v4l2_streamparm params;
    params.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    params.parm.capture.timeperframe.numerator = 1;
    params.parm.capture.timeperframe.denominator = 15;

    if (ioctl(fd, VIDIOC_S_PARM, &params) < 0) {
        close(fd);
        throw std::runtime_error("Failed to specify FPS!"); 
    }

    std::cout << "Setting up device's buffer specifications ..." << std::endl;
    struct v4l2_requestbuffers bspecs;

    bspecs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    bspecs.memory = V4L2_MEMORY_MMAP;
    bspecs.count = 1;

    std::cout << "Telling device how to compute data queue" << std::endl;
    if (ioctl(fd, VIDIOC_REQBUFS, &bspecs) < 0) {
        if (EINVAL == errno) {
            std::cout << "Device does not support memory mappings!" << std::endl;
        }

        close(fd);
        throw std::runtime_error("Error in specifying VIDIOC_REQBUFS!"); 
    }

    struct v4l2_buffer data;
    data.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    data.memory = V4L2_MEMORY_MMAP;
    data.index = 0;

    if(ioctl(fd, VIDIOC_QUERYBUF, &data) < 0){
        close(fd);
        throw std::runtime_error("Could not get specs for allocated buffer!");
    }

    std::cout << "Setting up memory mapping ..." << std::endl;
    std::cout << "Buffer length is " << data.length << "!" << std::endl;
    void* data_buffer = (char*)mmap(
        nullptr, 
        data.length, 
        PROT_READ | PROT_WRITE, 
        MAP_SHARED, 
        fd, 
        data.m.offset);
    struct Buffer buffer = { data_buffer, data.length };

    if (data_buffer == MAP_FAILED) {
        close(fd);
        throw std::runtime_error("Could not page-aligned memory allocate space for the buffer!");
    }

    return buffer;
}

void start_capturing(int fd) {
    struct v4l2_buffer data;
    data.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    data.memory = V4L2_MEMORY_MMAP;
    data.index = 0;

    if(ioctl(fd, VIDIOC_QUERYBUF, &data) < 0) {
        close(fd);
        throw std::runtime_error("Could not get specs for allocated buffer!");
    }

    std::cout << "Placing the buffer into device's in-queue!" << std::endl;
    if (ioctl(fd, VIDIOC_QBUF, &data) < 0) { 
        close(fd);
        throw std::runtime_error("Failed to capture!"); 
    } 

    std::cout << "Initiating streaming ..." << std::endl;
    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_STREAMON, &type) < 0) { 
        close(fd);
        throw std::runtime_error("Failed to start capturing!"); 
    }
}

cv::Mat read(int fd, const Buffer& buffer) { 
    struct v4l2_buffer data;
    data.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    data.memory = V4L2_MEMORY_MMAP;
    data.index = 0;

    if (ioctl(fd, VIDIOC_DQBUF, &data) < 0) { 
        close(fd);
        throw std::runtime_error("Could not read the frame!"); 
    }

    //char buffer[640 * 480];
    //auto num_bytes_written = fwrite(&data.index, data.bytesused, 1, stdout);
    //std::cout << "Number of bytes written: " << num_bytes_written << std::endl;
    cv::Mat frame(480, 640, CV_8UC1);
    std::cout << "Image allocated size: " << buffer.bytes << std::endl;
    memcpy((void*)frame.data, (void*)buffer.start, buffer.bytes);

    std::cout <<  "DEBUG" << std::endl;

    //usleep(1000);

    if (ioctl(fd, VIDIOC_QBUF, &data) < 0) { 
        close(fd);
        throw std::runtime_error("Failed to capture!"); 
    }

    return frame;
}

void stop_streaming(int fd) {
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_STREAMOFF, &type) < 0) { 
        close(fd);
        throw std::runtime_error("Failed to stop streaming"); 
    }
}

int main(void)
{
    int fd;
    std::cout << "Opening file descriptor" << std::endl;
    if ((fd = open("/dev/video2", O_RDWR)) < 0) { 
        throw("Error opening file descriptor!"); 
    }

    auto buffer = init(fd);
    
    start_capturing(fd);

    cv::namedWindow("test");

    std::cout << "Starting to get frames ..." << std::endl;
    while (1) {
        auto frame = read(fd, buffer);

        if (!frame.data) { 
            std::cout << "Frame is empty \n";
            continue; 
        }

        //cv::imwrite("test.jpg", frame);
        cv::imshow("test", frame);

        if (cv::waitKey(1) == 27) {
            break;
        }
    }
    
    cv::destroyAllWindows();

    stop_streaming(fd);

    close(fd);

    return EXIT_SUCCESS;
}

